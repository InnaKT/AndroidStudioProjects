package com.example.android.finalproject1eu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        Intent mapIntent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("https://www.google.no/maps/place/Aunegården/@69.6502907,18.9582721,15z/data=!4m5!3m4!1s0x0:0x7d3ace10102ddc6e!8m2!3d69.6502907!4d18.9582721"));
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
            Log.v("main activity", "starting activity");
        }
        else {Log.v("main activity","null");}
    }

    public void showMap(View view) {
        Log.v("main_activity","showMap method");
        Uri geoLocation= Uri.parse("https://www.google.no/maps/place/Aunegården/@69.6502907,18.9582721,15z/data=!4m2!3m1!1s0x0:0x7d3ace10102ddc6e?sa=X&ved=0ahUKEwiNw7u_1drRAhVLjCwKHT35DU4Q_BIIczAK");
        Log.v("main activity", "geolocation"+geoLocation);
            Intent intent = new Intent(Intent.ACTION_VIEW);
        Log.v("main activity", "intent");
            intent.setData(geoLocation);
        Log.v("main activity", "set data");
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
                Log.v("main activity", "starting activity");
            }
        else {Log.v("main activity","null");}
        }

    public void email(View view) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, "Aunegarden inquiry");
        intent.putExtra(Intent.EXTRA_EMAIL, "aunegarden@gmail.com");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    public void call(View view) {

        Intent intent = new Intent(Intent.ACTION_DIAL);

        intent.setData(Uri.parse("tel:" + "48502353"));

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }



}
