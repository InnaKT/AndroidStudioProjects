package com.example.android.geography;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
int score = 0;

    public void correct1(View view){
score=score+1;
        ImageView q1=(ImageView) findViewById(R.id.q1);
        q1.setImageResource(R.drawable.smile);
    }

    public void incorrect1(View view){
        ImageView q1=(ImageView) findViewById(R.id.q1);
        q1.setImageResource(R.drawable.angryface);
    }
    public void correct5(View view){
        score=score+1;
        ImageView q1=(ImageView) findViewById(R.id.q5);
        q1.setImageResource(R.drawable.smile);
    }

    public void incorrect2(View view){
        ImageView q1=(ImageView) findViewById(R.id.q2);
        q1.setImageResource(R.drawable.angryface);
    }
    public void correct2(View view){
        score=score+1;
        ImageView q1=(ImageView) findViewById(R.id.q2);
        q1.setImageResource(R.drawable.smile);
    }

    public void incorrect3(View view){
        ImageView q1=(ImageView) findViewById(R.id.q3);
        q1.setImageResource(R.drawable.angryface);
    }
    public void correct3(View view){
        score=score+1;
        ImageView q1=(ImageView) findViewById(R.id.q3);
        q1.setImageResource(R.drawable.smile);
    }

    public void incorrect4(View view){
        ImageView q1=(ImageView) findViewById(R.id.q4);
        q1.setImageResource(R.drawable.angryface);
    }
    public void correct4(View view){
        score=score+1;
        ImageView q1=(ImageView) findViewById(R.id.q4);
        q1.setImageResource(R.drawable.smile);
    }

    public void incorrect5(View view){
        ImageView q1=(ImageView) findViewById(R.id.q5);
        q1.setImageResource(R.drawable.angryface);
    }
}
