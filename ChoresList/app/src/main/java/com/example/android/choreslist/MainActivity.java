package com.example.android.choreslist;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    int scoreAnna = 0;
    int scoreFedor = 0;
    int scoreSofia = 0;


    public void displayForAnna(int score) {
        TextView scoreView = (TextView) findViewById(R.id.AnnaScore);
        scoreView.setText(String.valueOf(score));
        if (scoreAnna==10) {
            displayPrizeAnna();
        }
        if (scoreAnna==10) {
            displayPrizeAnna();
        }
        if (scoreAnna==20) {
            displayPrizeAnna20();
        }
        if (scoreAnna==30) {
            displayPrizeAnna30();
        }

    }

    public void displayForFedor(int score) {
        TextView scoreView = (TextView) findViewById(R.id.FedorScore);
        scoreView.setText(String.valueOf(score));

        if (scoreFedor==10) {
            displayPrizeFedor();
        }
        if (scoreFedor==20) {
            displayPrizeFedor20();
        }
        if (scoreFedor==30) {
            displayPrizeFedor30();
        }
    }

    public void displayForSofia(int score) {
        TextView scoreView = (TextView) findViewById(R.id.SofiaScore);
        scoreView.setText(String.valueOf(score));
        if (scoreSofia==10) {
            displayPrizeSofia();
        }
        if (scoreSofia==20) {
            displayPrizeSofia20();
        }
        if (scoreSofia==30) {
            displayPrizeSofia30();
        }
    }

    public void addOneAnna(View v) {
        scoreAnna = scoreAnna + 1;
        displayForAnna(scoreAnna);
        v.setBackgroundColor(Color.GREEN);

    }


    public void displayPrizeAnna() {
        ImageView prizeView = (ImageView) findViewById(R.id.AnnaPrize);
        prizeView.setImageResource(R.drawable.prize);
    }

    public void displayPrizeAnna20() {
        ImageView prizeView = (ImageView) findViewById(R.id.AnnaPrize20);
        prizeView.setImageResource(R.drawable.medal);
    }
    public void displayPrizeAnna30() {
        ImageView prizeView = (ImageView) findViewById(R.id.AnnaPrize30);
        prizeView.setImageResource(R.drawable.star);
    }


    public void displayPrizeFedor() {
        ImageView prizeView = (ImageView) findViewById(R.id.FedorPrize);
        prizeView.setImageResource(R.drawable.prize);
    }

    public void displayPrizeFedor20() {
        ImageView prizeView = (ImageView) findViewById(R.id.FedorPrize20);
        prizeView.setImageResource(R.drawable.medal);
    }
    public void displayPrizeFedor30() {
        ImageView prizeView = (ImageView) findViewById(R.id.FedorPrize30);
        prizeView.setImageResource(R.drawable.star);
    }

    public void displayPrizeSofia() {
        ImageView prizeView = (ImageView) findViewById(R.id.SofiaPrize);
        prizeView.setImageResource(R.drawable.prize);
    }
    public void displayPrizeSofia20() {
        ImageView prizeView = (ImageView) findViewById(R.id.SofiaPrize20);
        prizeView.setImageResource(R.drawable.medal);
    }
    public void displayPrizeSofia30() {
        ImageView prizeView = (ImageView) findViewById(R.id.SofiaPrize30);
        prizeView.setImageResource(R.drawable.star);
    }

    public void addOneFedor(View v) {
        scoreFedor = scoreFedor + 1;
        displayForFedor(scoreFedor);
        v.setBackgroundColor(Color.GREEN);
    }

    public void addOneSofia(View v) {
        scoreSofia = scoreSofia + 1;
        displayForSofia(scoreSofia);
        v.setBackgroundColor(Color.GREEN);
    }

    public void reset(View v) {
        scoreAnna = 0;
        scoreFedor = 0;
        scoreSofia = 0;
        displayForAnna(scoreAnna);
        displayForFedor(scoreFedor);
        displayForSofia(scoreSofia);


    }

    public void resetColor(View v) {
        ViewGroup view = (ViewGroup) findViewById(R.id.chores_layout);
        int childCount = view.getChildCount();
        Log.v("main activity", "childCOunt: " + childCount);
        for (int i = 0; i < childCount; ++i) {
            ViewGroup child = (ViewGroup) view.getChildAt(i);
            int childChildCount = child.getChildCount();
            for (int i1=0; i1 < childChildCount; ++i1){
                View childChild = child.getChildAt(i1);
                Log.v("main activity", "childCOunt: " + childChildCount);
                if (childChild instanceof Button) {
                    childChild.setBackgroundColor(Color.LTGRAY);
                }
            }

        }
    }

}



