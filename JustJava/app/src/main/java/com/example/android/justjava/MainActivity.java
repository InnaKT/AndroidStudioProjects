package com.example.android.justjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}


    int scoreAnna=0;
    int scoreFedor=0;
    int scoreSofia=0;


    public void displayForAnna(int score) {
        TextView scoreView = (TextView) findViewById(R.id.AnnaScore);
        scoreView.setText(String.valueOf(score));
    }
    public void displayForFedor(int score) {
        TextView scoreView = (TextView) findViewById(R.id.FedorScore);
        scoreView.setText(String.valueOf(score));
    }
    public void displayForSofia(int score) {
        TextView scoreView = (TextView) findViewById(R.id.SofiaScore);
        scoreView.setText(String.valueOf(score));
    }


    public void addOneAnna(View v) {
        scoreAnna=scoreAnna+1;
        displayForAnna(scoreAnna);
    }
    public void addOneFedor(View v) {
        scoreFedor=scoreFedor+1;
        displayForFedor(scoreFedor);
    }
    public void addOneSofia(View v) {
        scoreSofia=scoreSofia+1;
        displayForSofia(scoreSofia);
    }
    public void reset(View v) {
        scoreAnna = 0;
        scoreFedor = 0;
        scoreSofia=0;
        displayForAnna(scoreAnna);
        displayForFedor(scoreFedor);
        displayForSofia(scoreSofia);

    }
}
