// Add your package below
package com.example.android.justjava;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {
    int numberOfCoffees = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        EditText nameInput = (EditText) findViewById(R.id.name_input);
        String name = nameInput.getText().toString();
        CheckBox whippedCreamCheckbox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        boolean hasWhippedCream=whippedCreamCheckbox.isChecked();
        CheckBox chocolateCheckbox = (CheckBox) findViewById(R.id.chocolate_checkbox);
        boolean hasChocolate=chocolateCheckbox.isChecked();




        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, "Just Java order for"+name);
        intent.putExtra(Intent.EXTRA_TEXT,createOrderSummary(name, numberOfCoffees, hasWhippedCream, hasChocolate) );
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }


    }

    public void increment(View view) {

        if (numberOfCoffees==100){
            Context context = getApplicationContext();
            CharSequence text = "You cannot have more than 100 coffees!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return;
        }
        numberOfCoffees = numberOfCoffees + 1;
        display(numberOfCoffees);
    }

    public void decrement(View view) {
        if (numberOfCoffees==1){
            Context context = getApplicationContext();
            CharSequence text = "You cannot have less than 1 coffee!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return;
        }

        numberOfCoffees = numberOfCoffees - 1;
        display(numberOfCoffees);

    }

    private int calculatePrice(boolean hasWhippedCream, boolean hasChocolate) {
        int pricePerCoffee=5;
        display(numberOfCoffees);
        if (hasWhippedCream) {
            pricePerCoffee=pricePerCoffee+1;
        }
        if (hasChocolate) {
            pricePerCoffee=pricePerCoffee+2;
        }
        return numberOfCoffees * pricePerCoffee;
    }


    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }



    private String createOrderSummary (String name, int n, boolean w, boolean c){

        int priceTotal = calculatePrice(w, c);

        return "Name: "+name+"\n"+numberOfCoffees+" coffees" +"\nTotal: $"+n+"\nWhipped Cream? " + w +"\nChocolate? "+c+"\nThank you!";
    }
    /**
     * This method displays the given price on the screen.
     */

}